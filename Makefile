PL_VOL=17
# Both volumes listed at patristica.net redirect to this ID, which has
# page numbers that correspond to columns.
PL_URL='https://books.google.com/books?id=gPgUAAAAQAAJ&hl=it&pg=PA%d\#v=onepage&q&f=false'

PYTHON=python3
PANDOC_ARGS=-f html -t markdown --atx-headers --lua-filter preprocess.lua -M linkFormat:$(PL_URL) -M volume:$(PL_VOL)
MKRTFNOTES=../acc-great-commentary/code/mkRTFnotes.py

BOOKS=Rom.md 1Cor.md 2Cor.md Gal.md Eph.md Phl.md Col.md 1Th.md 2Th.md 1Tim.md 2Tim.md Tit.md Phlem.md

notes: $(BOOKS) template.rtf
	rm -rf build
	mkdir -p build/notes
	$(PYTHON) $(MKRTFNOTES) $(BOOKS) --template template.rtf -o build/notes

Rom.md: tei-xml/AmbrosiasterRomPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:Romans -o $@

1Cor.md: tei-xml/Ambrosiaster1CorPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:1Corinthians -o $@

2Cor.md: tei-xml/Ambrosiaster2CorPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:2Corinthians -o $@

Gal.md: tei-xml/AmbrosiasterGalPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:Galatians -o $@

Eph.md: tei-xml/AmbrosiasterEphPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:Ephesians -o $@

Phl.md: tei-xml/AmbrosiasterPhlPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:Philippians -o $@

Col.md: tei-xml/AmbrosiasterColPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:Colossians -o $@

1Th.md: tei-xml/Ambrosiaster1ThPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:1Thessalonians -o $@

2Th.md: tei-xml/Ambrosiaster2ThPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:2Thessalonians -o $@

1Tim.md: tei-xml/Ambrosiaster1TimPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:1Timothy -o $@

2Tim.md: tei-xml/Ambrosiaster2TimPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:2Timothy -o $@
	
Tit.md: tei-xml/AmbrosiasterTitPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:Titus -o $@

Phlem.md: tei-xml/AmbrosiasterPhmPL.xml tei2html.py preprocess.lua
	$(PYTHON) tei2html.py $< - | pandoc $(PANDOC_ARGS) -M bookname:Philemon -o $@
