This project converts TEI XML of Ambrosiaster's commentaries on the Pauline corpus into RTF notes ready for import into [Accordance Bible Software](https://www.accordancebible.com).

## The text

The text of the commentary comes from *Patrologia latina* (PL) vol. 17
(1845). It has been superseded for scholarly purposes by CSEL 81, but
the former is out of copyright and the latter is not. The PL text can
also be conveniently downloaded from the Corpus Corporum website in TEI
XML. Those XML files were prepared by Dr. Philipp Roelli and are
licensed under a Creative Commons Attribution-NonCommercial-ShareAlike
4.0 International ([CC BY-NC-SA
4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)) license. The
same license applies to the Accordance User Notes, but no additional
attribution is needed: it is sufficient to credit Dr. Philipp Roelli.

The author of this commentary is conventionally called Ambrosiaster,
because he was originally thought to be St. Ambrose of Milan, and later
proved to be someone else. There are multiple theories on who that
someone else might have been. The idea that he was a Roman priest
writing in the late fourth century seems fairly plausible.

The textual history is somewhat complicated. There are three different
recensions (α, β, and γ) of the commentary on Romans, and two recensions (α
and γ) of the commentary on the other 12 letters Ambrosiaster attributed
to St. Paul. Who is responsible for the later revisions? According to H.
J. Vogels's reconstruction (1966), which was corroborated by Theodore S.
de Bruyn (2017), it was Ambrosiaster himself. He first wrote a
commentary on Romans (recension α). Then he commented the remaining
epistles (recension α) and revised his commentary on Romans (recension β)
in the light of his increased knowledge of St. Paul's teaching. Finally,
he revised all the commentaries, making the language clearer and
occasionally expanding the commentary (recension γ).

Based on some spot checks I made of the commentary on 1 Corinthians, it
looks like the PL edition generally corresponds to recension γ. This
makes sense, because given the choice, an Early Modern editor would
prefer the clearest and most complete manuscript text available.

G.L. Bray's translation in the Ancient Christian Texts series (2009)
assumes that recension α was Ambrosiaster's real commentary, while
recensions β and γ were the work of someone else, who sometimes
contradicted Ambrosiaster. Bray made use of the later recensions only
when recension α was too obscure to translate without aid.

## Required Software

* [pandoc](https://pandoc.org)
* Python 3.6 or later (required for f-string support)
    * `pip install roman`
    * `pip install beautifulsoup4`
* `mkRTFnotes.py` from the [acc-great-commentary repo](https://gitlab.com/jlmp/acc-great-commentary).
* Accordance on macOS for importing. It should also work on Windows, but I haven't tested it.

With everything in place, just type `make` in Terminal to build the notes. Then you can import them in Accordance with File > User Files > Import Notes Folder…

## Limitations

* At present, the conversion is completely automatic. The tagging of Scripture references is imperfect.
    * It may be desirable to tag the remaining references manually. This would require editing the Markdown files for each book produced by the Makefile. If you plan to do this, it would be advisable to add those files to the repo and adjust the Makefile so that they will not be overwritten.
* `tei2html.py` is adding spaces around the Scripture references it tags. This is happening inside BeautifulSoup. Perhaps avoiding `prettify` or using `formatter=None` would fix this.
