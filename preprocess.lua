-- Prepare Ambrosiaster PL texts for mkRTFnotes.py

-- set from Meta
local bookname = nil
local linkFormat = nil
local volume = nil
-- initialized to 0 in case there's no PROLOGUS.
local chapter = 0


function getMetadata(m)
  bookname = m.bookname
  if bookname == nil then
    print("bookname must be defined in metadata (or with -M).")
    os.exit(1)
  end
  -- A missing link format is treated as a fatal error, even though
  -- we could just not link page numbers.
  linkFormat = m.linkFormat
  if linkFormat == nil then
    print("linkFormat must be defined in metadata (or with -M).")
    os.exit(1)
  end
  volume = m.volume
  if volume == nil then
    print("volume must be defined in metadata (or with -M).")
    os.exit(1)
  end
  -- Delete metadata we don't need in the output.
  m.bookname = nil
  m.linkFormat = nil
  m.volume = nil
end


function transformHeader (el)
  -- takes a heading and changes it as required for output.
  local heading = pandoc.utils.stringify(el.content)
  if heading:find('PROLOGUS') ~= nil then
    chapter = 0
    return pandoc.Header(1, pandoc.Str(bookname))
  else
    -- Just count headings instead of parsing the content of the header
    -- ("CAPUT PRIMUM", "CAPUT II.", "CAPUT III.", etc.).
    chapter = chapter + 1
    heading = string.format("%s %d", bookname, chapter)
    return pandoc.Header(2, pandoc.Str(heading))
  end

  return el
end


function tagVerses(el)
  -- Takes a paragraph element. If it finds "(Vers. %d" in the
  -- text, it prefixes a level 3 header containing the verse reference.
  local verse = pandoc.utils.stringify(el.content):match("%(Vers. (%d+)")
  if verse then
    local heading = string.format("%s %d:%s", bookname, chapter, verse)
    return {pandoc.Header(3, pandoc.Str(heading)), el}
  end
end


function handlePageNumber(el)
  -- If the span has the pnum class, turn it into a link to
  -- a scanned volume of PL. linkFormat is set above.
  if el.classes:includes("pnum") then
    local locator, colnum
    locator, colnum = pandoc.utils.stringify(el.content):match("((%d+)[ABCD])")
    if locator==nil then
      print(pandoc.utils.stringify(el.content))
      os.exit(1)
    else
      -- Round column down to an odd number to get a valid page number
      -- for Google.
      local url = string.format(linkFormat, colnum - (colnum+1)%2)
      local content = string.format("[%d:%s]", volume, locator)
      return pandoc.Link(content, url)
    end
  end
end

return {
  {Meta = getMetadata},
  {Header = transformHeader, Para = tagVerses, Span = handlePageNumber},
}
