<META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=utf-8"><title>017_Ambrosiaster_Commentaria-in-Epistolam-ad-Thessalonicenses-Secundam</title>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TEI.2 SYSTEM 'teixlite.dtd'>
<TEI.2>
  <teiHeader>
     <fileDesc>
        <publicationStmt>
           <idno>Ambros.CoInEpA9</idno>
        </publicationStmt>
        <seriesStmt>
           <title>Patrologia Latina</title>
        </seriesStmt>
        <notesStmt>
           <note>17</note>
        </notesStmt>
        <sourceDesc>
           <bibl>
              <author ref="http://viaf.org/viaf/281829495">Ambrosiaster<date>366-384</date></author>
              <title>Commentaria in Epistolam ad Thessalonicenses Secundam</title>
              <editor></editor>
              <extent></extent>
              <imprint>
                 <pubPlace>Parisiis</pubPlace>
                 <publisher>J. P. Migne</publisher>
                 <date>1845</date>
              </imprint>
              <edition>early modern edition, no apparatus</edition>
           </bibl>
        </sourceDesc>
     </fileDesc>
     <encodingDesc>
        <projectDesc>this file was encoded in TEI xml for the University of Zurich's <i>Corpus Corporum</i> project (www.mlat.uzh.ch) by Ph. Roelli in 2013</projectDesc>
         <editorialDecl>
            <normalization>Classical Latin orthography</normalization>
         </editorialDecl>
     </encodingDesc>
     <profileDesc>
        <creation>
           <date></date>
        </creation>
        <langUsage>
           <language>latin</language>
        </langUsage>
        <textClass>
           <keywords>
              <list>
                 <item></item>
              </list>
           </keywords>
           <keywords scheme='genre'>
              <list>
                 <item></item>
              </list>
           </keywords>
           <keywords scheme='period'>
              <list>
                 <item></item>
              </list>
           </keywords>
        </textClass>
     </profileDesc>
   </teiHeader>
<text>

<div2><head>PROLOGUS.</head>

<pb n="0453C"/>
<p><pb n="0453D"/>Quoniam in prima epistola ad Thessalonicenses 
data inter caetera etiam de adventu Domini quaedam 
scripsit, et de resurrectione sanctorum; nunc aliam 
scribit ad eos, in qua significat, licet obscure (neque 
enim aperte potuit scribere), de abolitione regni Romani, 
et de antichristi apparentia et damnatione, et 
de quorumdam fratrum inquietudine.</p>
</div2>

<div2><head>CAPUT PRIMUM.</head>
<p> <i>(Vers. 1-3.) Paulus, et Silvanus, et Timotheus Ecclesiae 
Thessalonicensium in Deo Patre nostro, et in 
Domino Jesu Christo. Gratia vobis et pax a Deo Patre 
nostro, et Domino Jesu Christo.</i> Solemnibus verbis 
scribit ad eos; solito enim more sermonum caput continetur 
epistolae: et idem ipsi tres sunt, quorum nomine 
scribitur, sicut et prima epistola <i>(I Thess. I, 1)</i>.</p>
<p> <i>Gratias agere debemus Deo semper de vobis, fratres, 
<pb n="0454C"/>
sicut dignum est; quoniam superexcrescit fides 
<pb n="0454D"/>
vestra, et multiplicatur charitas uniuscujusque vestrum 
omnium in vos invicem.</i> Deo se debitores de 
horum profectu in gratiarum actione profitentur; 
et ut non qualecumque esse debitum ostenderet: <i>sicut 
dignum est,</i> ait; ut pro tam infinito dono magnas gratias 
referendas Deo testarentur. Post acceptam enim <B>285</B> 
 primam epistolam meliores esse, quam fuerant, 
perstiterunt; ita ut in eo copiosi et abundantes viderentur, 
quod post, Domini et Dei devotionem habemus 
mandatum, ut diligamus nos invicem. <i>(Joan. XIII, 34.)</i> </p>
<p> <i>(Vers. 4, 5.) Ita ut nos ipsi de vobis gloriemur in Ecclesiis 
Dei pro patientia vestra et fide in omnibus persecutionibus 
vestris et pressuris, quas sustinetis in exemplum 
justi judicii Dei, in hoc ut digni habeamini regno 
Dei, pro quo et patimini.</i> Gloriam esse suam in horum 
conversatione et persecutione tolerata testatur, in 
<pb n="0455A"/>
tantum ut apud caeteras Ecclesias exempla eorum 
proferantur; ut imitatione eorum crescant in devotione 
passionum pro Christo, et mereantur consequi 
quae promissa sunt sanctis, in die justi judicii Dei, 
cujus amore mundum spernere non dubitarunt. Ipsae 
enim pressurae exempla sunt futurorum meritorum in 
his, qui patiuntur, ad gloriam: in illis autem, qui 
persequuntur, ad perditionem.</p>
<p> <i>(Vers. 6-9.) Siquidem justum est apud Deum retribuere 
eis, qui vos deprimunt, pressuram: et vobis 
qui afflictionem sustinetis, requiem nobiscum in revelatione 
Domini Jesu de coelo cum angelis virtutis ejus 
in igne flammae dantis vindictam in eos, qui non noverunt 
Deum, et in eos qui non obaudiunt Evangelio 
Domini nostri Jesu Christi, qui poenas solvent in interitum 
<pb n="0455B"/>
aeternum a facie Domini, et a gloria virtutis 
ejus.</i> Quoniam exemplum justi judicii Dei apparere 
dixit sive in bonos, sive in malos; idcirco subsecutus 
ait: <i>Siquidem justum est apud Deum;</i> ut 
reddat unicuique secundum opera sua. Quid tam 
justum, quam ut hi qui in saeculo deprimunt bonos, 
et extorres eos faciunt persecutionibus, in futuro 
eadem patiantur, quae faciunt: illi autem ut sint in 
requie cum caeteris sanctis, qui de magna tribulatione 
venientes, regnum accipient aeternum in adventu 
Domini de coelo; cum coeperit venire cum 
coelesti exercitu suo et ministro igne ad dandam 
vindictam in paganos, qui ignorant Deum Patrem 
esse Jesu Christi; et Judaeos, qui dicentes se Deum 
scire, non credunt Evangelio Christi, quod dedit 
<pb n="0455C"/>
illi Pater Deus, ut traderet servis suis? Quos omnes 
praesentia Domini, et gloria majestatis ejus ignis 
exuret; ut debitas reddant poenas interitus in aeternum, 
 semper patientes, non tamen penitus deficientes; 
ut ipsa poena illos quodammodo generet 
semper, quo consumantur.</p>
<p> <i>(Vers. 10.) Cum venerit clarificari in sanctis suis, et 
mirificari in omnibus qui crediderunt; quia fidem habuit 
testimonium nostrum super vos in die illa adventus 
Domini.</i> Duplicem continet sensum; adveniet enim 
punire malos, et glorificare bonos. Ipse enim clarus 
et mirabilis videbitur in eos, qui credunt exemplo 
et fidei apostolorum, cum fuerint coronati, testimonium 
reddente illis Evangelio in die Domini: severus 
autem apparebit in incredulos, cum aeternis 
<pb n="0455D"/>
poenis coarctari coeperint. Corona enim discipulorum 
magistri est gloria, et veritas ejus incredulorum 
est poena; quia quod praedicabatur, non credebatur.</p>
<p> <i>(Vers. 11, 12.) In quo et oramus semper pro vobis, ut 
vos dignos habeat vocatione sua Deus noster, et impleat 
omni placito bonitatis, et opere fidei in virtute; ita ut 
clarificetur nomen Domini nostri Jesu Christi in vobis, 
et vos in ipso secundum gratiam Dei nostri, et Domini 
Jesu Christi.</i> Gaudio addit et preces, ut quia devoti 
sunt, dignetur Deus adjuvare eos, ut perficiant opus 
<pb n="0456A"/>
coeptum ad claritatem Domini nostri Jesu Christi; 
ut perseverantia discipulorum testimonium sit magistro 
ad gloriam. Auctoritas enim magistri in discipulorum 
est fiducia, cum se in accepta gratia faciunt 
digniores.
</p></div2>

<div2><head>CAPUT II.</head>
<p> <i>(Vers. 1-4.) Rogamus autem vos, fratres, per adventum 
Domini nostri Jesu Christi, et nostrae congregationis 
in illum, ne facile moveamini a sensu vestro, 
neque conturbemini, neque per spiritum, neque per 
verbum, neque per epistolam tamquam a nobis missam, 
quasi instet dies Domini. Ne quis vos seducat ullo 
modo; quoniam nisi venerit defectio primum, et revelatus 
fuerit homo ille peccati, filius perditionis, qui 
adversatur, et extollitur supra omne, quod dicitur 
<pb n="0456B"/>
Deus, aut quod colitur; ita ut in templo Dei sedeat, 
ostentans se ipsum, quasi sit Deus.</i> Obsecrat eos ne 
leviter et facile de adventu quasi imminentis Domini 
opinionem reciperent: et talia dat mandata, ut 
etiamsi per spiritum aliquis velut propheta fuerit 
locutus, credi ei non debeat; neque si per tractatum 
aut per epistolam nomine forte apostolorum 
scriptam, assensum <B>286</B> tribuendum. Solent enim 
tergiversatores, ut fallant, sub nomine clari alicujus 
viri epistolam fingere; ut auctoritas nominis 
possit commendare, quod per se ipsum recipi non 
posset. Hoc ideo, ne ipsa perturbatione dum incauti 
invenirentur, possent seduci ad adorandum diabolum, 
qui idcirco haec jactabit, ut sub Salvatoris nomine 
apparens, ac fallens sanctos, velit se adorari, 
<pb n="0456C"/>
ut decipiat credentes in Christum. Sed ut locum 
aut occasionem fallendi, quam se putat habere, non 
habeat impudentissimus Satanas, tempus et signa 
adventus Domini designavit; quia non prius veniet 
Dominus, quam regni Romani defectio fiat, et appareat 
antichristus, qui interficiet sanctos, reddita 
Romanis libertate, sub suo tamen nomine. Sciens 
enim venturum Dominum ad se comprimendum, 
nomen ejus sibi usurpabit: et ut regnum ejus 
verum esse videatur, attrahet secum qui simul 
cum eo pereant; ita ut in domo Domini in sede 
sedeat Christi, et ipsum Deum se asserat, non 
Filium Dei. Unde in Evangelio ad Judaeos Dominus 
ait: <i>Ego veni in nomine Patris mei, et non me recepistis; 
si alius venerit in nomine suo, illum accipietis</i> 
<pb n="0456D"/> <i>(Joan. V, 43)</i>.
Quamobrem ex circumcisione, aut 
circumcisum illum venire sperandum est; ut sit 
Judaeis credendi illi fiducia. Itaque cum Thessalonicensibus 
scribit Apostolus, docet omnes in re hujuscemodi 
cautos esse debere.</p>
<p> <i>(Vers. 5, 6.) Annon meministis quod me adhuc 
apud vos agente, haec dicebam vobis? Et nunc quid 
detineat, scitis: ad hoc ut reveletur ille in suo tempore.</i> 
Hoc superesse dicit, ut reveletur ille, qui 
ante Dominum venturus est; ut jam speretur et Dominus 
venturus, quod supra quasi sub velamine 
<pb n="0457A"/>
factus est, dicens: <i>Nisi venerit defectio primum;</i> 
quam regni Romani abolitionem superius intelligendam 
memoravi; ut cum defecerit, et venerit 
antichristus, tunc adventus Domini venire credatur.</p>
<p> <i>(Vers. 7.) Nam mysterium jam operatur iniquitatis, 
tantum ut qui nunc tenet, teneat, quoadusque de 
medio fiat.</i> Mysterium iniquitatis a Nerone inceptum 
est, qui zelo idolorum apostolos interfecit, instigante 
patre suo diabolo, usque ad Diocletianum, 
et novissime Julianum, qui arte quadam et subtilitate 
coeptam persecutionem implere non potuit; 
quia desuper concessum non fuerat. His enim ministris 
utitur Satanas, ut interim sub turba deorum 
ad seducendos homines unius veri Dei manifestatione 
illudat, quamdiu steterit regnum Romanum, 
<pb n="0457B"/>
hoc est, quod dicit: <i>Donec de medio fiat.</i> </p>
<p> <i>(Vers. 8, 9.) Et tunc revelabitur ille iniquus, quem 
Dominus Jesus interficiet spiritu oris sui, et destruet 
illustratione praesentiae suae: cujus est adventus secundum 
operationem Satanae, in omni potestate, et 
signis et portentis mendacii.</i> Post defectum regni 
Romani appariturum antichristum dicit, sicut memoratum 
est. Novissimo enim tempore sciens diabolus 
imminere sibi interitum; deficiente autem 
regno Romano, et ille de coelis mittetur deorsum, 
projectus in terram (sicut dictum est in Apocalypsi 
Joannis apostoli) subornabit sibi in quo et per quem 
signa quaedam virtutis permissu justi Dei faciens, 
se commendet, ut adoretur, quasi sit Deus. Imitabitur 
enim Deum, ut sicut Filius Dei divinitatem 
<pb n="0457C"/>
suam homo natus vel factus signis ac virtutibus demonstravit; 
ita et Satanas in homine apparebit, ut 
virtutibus mendacii ostendat se Deum, sicut supra 
dictum est. Revelatio vero mysterii iniquitatis haec 
est; cum enim apparuerit antichristus, cognoscetur 
ipse esse quasi eorum Deus, quos prius nutu ejus 
ut deos coluit vulgus, quorum sit ipse primus aut 
summus, quod signorum virtutibus faciet credibile. 
Ideo vero portenta mendacii dixit, quia hoc se vult 
credi per signa, quod non est; ut decipiat quos circumventurus 
est: quod sanctis ideo praedictum est, 
ut sciant quid caveant.</p>
<p> <i>(Vers. 10.) Et in omni fallacia iniquitatis, iis qui 
pereunt: pro eo quod dilectionem veritatis non susceperunt, 
ad hoc ut salvi fierent.</i> Iis dicit fallaciam 
<pb n="0457D"/>
iniquitatis prodigiorum ejus proficere, qui perituri 
sunt; quia enim veritatis charitatem spreverunt, per 
quam salvari potuerant apostolis praedicantibus, 
traditi sunt diabolo; nolentes enim salvari, deseruntur 
a Deo.</p>
<p> <i>(Vers. 11.) Et idcirco mittet illis Deus operationem 
erroris, in hoc ut credant mendacio; quatenus judicentur 
omnes, qui non crediderunt veritati, sed consenserunt 
iniquitati.</i> Operatio erroris est falsis fidem 
committere, ut qui claritatem luminis <B>287</B> diem 
esse consentire noluerunt, putant tenebras diem 
dici debere; ut possint propensiori delicto rei facti, 
sine contradictione damnari veritatis inimici, fautores 
autem iniquitatis.</p>
<p><pb n="0458A"/> <i>(Vers. 12, 13.) Nos vero debemus gratias agere Deo 
semper de vobis, fratres dilecti a Domino, quia assumpsit 
vos Deus a principio ad salutem, in sanctificationem 
spiritus, et fidem veritatis, in quam vocavit vos per 
Evangelium nostrum in acquisitionem gloriae Domini 
nostri Jesu Christi.</i> Hoc loco praescientia Dei personat, 
quia scit omnium mentes ante nativitatem eorum; 
nec enim latet illum, qui credituri sunt. Idcirco 
a principio scit hos fideles fore, qui credentes 
augmentum fidei faciunt, non detrimentum; sanctificati 
enim per fidem veritatis Evangelii Filii Dei, 
acquiruntur ad augmentum gloriae corporis Christi, 
sicut et in alia epistola ait: ut crescant, inquit, omnia 
in incrementum Dei <i>(Coloss. II, 19)</i>. Quicumque 
enim, deserto diabolo, confugiunt ad fidem Christi, 
<pb n="0458B"/>
augmentum faciunt Deo in corpore Christi, qui prius 
fecerant detrimentum.</p>
<p> <i>(Vers. 14.) Itaque, fratres, state, et tenete traditiones 
nostras, quas didicistis sive per verbum, sive per 
epistolam nostram.</i> Ut praescientia Dei maneat in salutem 
illorum, idcirco in traditione Evangelii standum 
ac perseverandum monet; ut solliciti sint, ne 
desidia et otio torpentes in operibus Dei, non impleant 
quod coeperunt. Sic enim praescit credere et 
posse manere, si crebra admonitione pulsentur.</p>
<p> <i>(Vers. 15, 16.) Ipse autem Dominus noster Jesus 
Christus, et Deus ac Pater noster, qui dilexit nos, et 
dedit consolationem aeternam, et spem bonam in gratia; 
consoletur corda vestra, et stabiliat in omni opere 
et verbo bono.</i> Quoniam Pater et Filius una virtus, 
<pb n="0458C"/>
unaque divinitas et substantia est, ideo non dubitavit 
primo Dominum nostrum Jesum Christum nominare, 
deinde Deum Patrem nostrum dignatione 
ejus, non veritate naturae: qui in tantum dilexit 
nos, ut Filium suum daret pro nobis <i>(Joan. III, 16)</i>, 
Deum pro hominibus, pro servis Dominum, pro 
adoptivis Filium verum; ut mors ejus vita nostra 
sit, et resurrectio ejus justificatio nostra, secundus 
autem adventus ejus requies vitae nostrae, et gloria 
in aeternum; ut ista spes consolatio sit praesentium 
pressurarum, per quam fundati crescerent in bonis 
operibus et doctrina.</p>
</div2>

<div2><head>CAPUT III.</head>
<p> <i>(Vers. 1, 2.) Quod reliquum est, fratres, orate pro 
<pb n="0458D"/>
nobis, ut verbum Domini currat et clarificetur, sicut 
et apud vos, et ut liberemur ab importunis et nequam 
hominibus; non enim omnium est fides.</i> De caetero 
orandum hortatur, ut dignetur Deus doctrinam suam 
infatigabili cursu dirigere, et transfundere per os 
Apostoli sui in aures audientium, et ut compesceret 
et sedaret malorum hominum seditiones, qui diffidunt 
veritati, et loquendi fiducia cresceret Apostolo 
ad ejus fructum, et conversionem multorum.</p>
<p> <i>(Vers. 3.) Fidelis autem Deus, qui stabiliet eos, et 
custodiet a malo.</i> Quoniam Deus fidelibus et bonae 
vitae hominibus auxilium suum non defuturum in 
necessitate promisit ad confirmationem illorum, ii 
autem tam in fide quam in opere aemuli erant bonorum; 
<pb n="0459A"/>
idcirco fidelem Deum dicit in promissis ejus, 
quia providentia ejus esset circa eos.</p>
<p> <i>(Vers. 4.) Confidimus autem in Domino de vobis, 
quoniam quae praecepimus, et facitis, et facietis.</i> Propter 
quod mereri illos, ut Dei defensione totus 
praestari confideret; idcirco ea quae illis praecipiebat 
in nomine Domini, fieri ab illis et futura esse non 
dubitat.</p>
<p> <i>(Vers. 5.) Dominus autem dirigat corda vestra in 
dilectione Dei, et patientia Christi.</i> Hoc illis exoptat 
a Deo, quod non ambigit posse praestari; ut in Dei 
charitate cor eorum proficeret ad exspectandum adventum 
Domini, ut condemnato saeculo, desideretur 
adventus ejus, qui saeculum vicit.</p>
<p> <i>(Vers. 6.) Denuntiamus autem vobis, fratres, in 
<pb n="0459B"/>
nomine Domini nostri Jesu Christi, secerni vos ab omni 
fratre intemperanter ambulante, et non secundum traditionem, 
quam accepistis a nobis.</i> Quoniam in omni 
plebe non omnes obedientes sunt verbo doctrinae, 
non legis, sed suum placitum et consilium sequentes; 
propter hoc declinandum ab his praecipit, ut 
cognoscant semet ipsos errare.</p>
<p> <i>(Vers. 7, 8, 9.) Ipsi enim scitis quemadmodum debeatis 
imitari nos, quoniam non intemperanter viximus 
inter vos, neque gratis panem edimus a quoquam; sed 
in labore et fatigatione,</i> <B>288</B> <i>nocte dieque operantes, ad 
hoc ne graves essemus cuiquam vestrum: non quia non 
habuerimus potestatem, sed ut nosmet ipsos formam daremus 
vobis ad imitandos nos.</i> Illos dicit non ignaros 
operis et laboris sui, quem sedulo exitu operabatur, 
<pb n="0459C"/>
ne pane indigeret alieno, ut exemplum daret eis 
ad imitandum illum. Neque enim reprehensibilis 
esset, si panem ab his reciperet ad corporis curam, 
qui animis illorum escam spiritalem praebebat, quae 
etiam corpori immortalitatem cum gloria procuraret. 
Tunc etenim exerta potest esse libertas, si semet 
ipsum quis ventris causa aut pecuniae nulli subjiciat, 
suo pane contentus; maxime quod ad laudem magis 
proficit, si cum liceat, et sit sumendi potestas, contineat: 
hic talis prodesse potest audientibus se; reverentiam 
enim habebunt rigoris hujus, et ea quae 
ab hoc mandantur, praetermitti non poterunt.</p>
<p> <i>(Vers. 10.) Nam cum essemus apud vos, hoc praecipiebamus 
vobis, quod si quis non vult operari, nec manducet.</i> 
Non solum verbis docebat, sed hortabatur et 
<pb n="0459D"/>
factis; idonei enim magistri est, si ea quae verbis 
docet, operibus expleat. Tunc etenim addiscentes 
vera esse sciunt, quae audiunt, si negligi illa non 
viderint a doctore. Quamvis enim manifeste vera 
esse non ignorentur, quae docentur; tamen si negligi 
coeperint a magistro, difficile proficient audientibus: 
magis enim opera suadent, quam verba. Quamobrem 
magnis praemiis remunerandi sunt, qui negligentibus 
magistris, de solis verbis proficiunt tam in 
opere quam in sensu. Forma ergo erat iis, qui mediocris 
<pb n="0460A"/>
vel tenuis erant substantiae in plebe; ut discerent 
quatenus libertatem suam non amitterent. 
Unde et Salomon: <i>Raro,</i> inquit, <i>inferes pedem ad 
amicum tuum, ne saturatus tui oderit te (Prov. XXV, 
17)</i>. Qui enim frequenter ad alienam mensam convenit, 
otio deditus aduletur necesse est pascenti se; 
cum religio nostra ad libertatem homines advocet. 
Ideoque si quis, inquit, manducare spernit, cesset 
ab opere: sed quia nullus sine cibo potest vivere, 
det operam laborandi; ut arbitrio suo vivens Deum 
possit habere propitium.</p>
<p> <i>(Vers. 11.) Audivimus enim quosdam ambulare inter 
vos intemperanter, nihil operantes, sed curiose agentes</i> 
Qui otiosi esse desiderant, id agunt quatenus occasio 
nascatur, qua introitum suum desiderabilem faciant 
<pb n="0460B"/>
domibus divitum, ambulantes gesta et opiniones verborum 
subtiliter colligunt, scientes quid de quo velint 
audire, ut libenter et requisiti pascantur. Quod factum 
valde abhorret a disciplina Dominica; horum 
enim venter Deus est, qui foeda cura necessaria provident.</p>
<p> <i>(Vers. 12.) His igitur qui hujuscemodi sunt, denuntiamus, 
et exhortamur in Domino Jesu Christo, ut 
cum quiete operantes suum panem manducent.</i> Ut eis 
facilius suadeat, interposito nomine Domini Jesu 
Christi, exhortatur eos, ut obaudiant, quod eis sit 
utile.</p>
<p> <i>(Vers. 13, 14, 15.) Vos autem, fratres, nolite 
deficere in benefaciendo. Quod si quis non obedit verbo 
nostro per epistolam, hunc notate, et nolite cum eo 
<pb n="0460C"/>
conversari, ut reverentia confundatur: et non velut 
inimicum existimetis, sed admonete consiliis, ut fratrem.</i> 
Propter quod superius unumquemque suum 
panem manducare debere praecepit; ne forte quidam 
illorum occasionem parcitatis et inhumanitatis captarent; 
ideo statim subjecit: <i>Vos autem, fratres, 
nolite defatigari benefacientes.</i> Neque enim in reprehensionem 
venit, qui humanus et largus est; sed 
hic qui cum possit laborem ferre, in otio vult vitam 
agere. Quem si epistolae auctoritas non corrigit, notandum 
hunc, et nec conversandum cum eo praecipit; 
ut confusus, qui ab omnibus evitatur, subjiceret se 
praeceptis Apostoli. Quod tamen sine iracundia et 
contumelia vult fieri, ut patienter declinetur ab eo; 
ita tamen, ut si res tulerit loqui cum eo, monendum 
<pb n="0460D"/>
illum, ut patiatur se corripi, nec ultra praecepta 
Apostoli contemnenda: familiaritatem quoque tamdiu 
cum eo non habere, quamdiu obaudiens fiat. Nam 
si ad iracundiam contumeliis lacessatur, pejor se fiet; 
incitabitur enim ad contentionem, et incipiet erratum 
suum velle defendere, post autem crubescere 
emendare se; ne quod prius excusabat, postea fateri 
videatur.</p>
<p> <i>(Vers. 16.) Ipse autem Deus pacis det vobis pacem 
semper in omni loco. Dominus sit cum omnibus vobis.</i> 
<pb n="0461A"/>
Semper quod suum est, bono voto illos prosequitur, 
quasi charissimos filios, optans semper cum illis 
pacem manere, quae est dux ad vitam aeternam. <B>289</B> 
Cum his enim Dominus se esse promisit, qui fuerint 
pacifici.</p>
<p> <i>(Vers. 17.) Ipsa salutatio mea manu Pauli, quod 
est signum in omni epistola: ita scribo.</i> Propter interpolas 
et adulteratores Scripturarum semper se manu 
<pb n="0462A"/>
sua in omni epistola sua salutationem <B>290</B> subscribere 
testatur; ut sub nomine ejus epistola accepto 
ferri non possit, quae non fuerit manu ejus 
subscripta.</p>
<p> <i>(Vers. 18.) Gratia Domini nostri Jesu Christi cum 
omnibus vobis.</i> Haec salutatio manu Apostoli conscripta 
est, qua gratiam Domini nostri Jesu Christi optat 
esse cum eis. Amen.
</p>

</div2></text>
</TEI.2>
