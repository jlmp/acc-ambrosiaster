# Quick and dirty script to convert Corpus Corporum TEI XML to HTML
# to feed into pandoc.

from bs4 import BeautifulSoup
import re
import roman
import sys
import argparse


def convert(soup):
    """Change tags and discard metadata to turn TEI into HTML.
    """
    content = soup.find('text')
    for tag in content.find_all(['div1', 'div2', 'div3']):
        # just discard divs instead of replacing them with div.
        tag.unwrap()
    for tag in content.find_all(['head']):
        tag.name = 'h2'
    for tag in content.find_all('pb'):
        page_number = tag['n']
        span = soup.new_tag('span')
        span.string = page_number
        span['class'] = 'pnum'
        tag.replace_with(span)
    content.name = 'body'
    return content


def mark_if_scripture(match):
    """
    Helper function used by mark_verse_refs() with re.sub().
    """
    # Recognized names and abbreviations.
    books = {
        'Gen':      'Gen.',
        'Genes':    'Gen.',
        'Ex':       'Ex.',
        'Exod':     'Ex.',
        'Num':      'Num.',
        'Levit':    'Lev.',
        'Deut':     'Deut.',
        'Josue':    'Josh.',
        'Judic':    'Judg.',
        'I Reg':    '1Sam.',
        'II Reg':   '2Sam.',
        'III Reg':  '1Kings',
        'IV Reg':   '2Kings',
        'I Par':    '1Chr.',
        'II Par':   '2Chr.',
        'Job':      'Job',
        'Ps':       'Psa.',
        'Psal':     'Psa.',
        'Psalm':    'Psa.',
        'Prov':     'Prov.',
        'Proverb':  'Prov.',
        'Eccl':     'Eccl.',
        'Eccles':   'Eccl.',
        'Sap':      'Wis.',
        'Esai':     'Is.',
        'Isai':     'Is.',
        'Jerem':    'Jer.',
        'Ezech':    'Ezek.',
        'Dan':      'Dan.',
        'Ose':      'Hos.',
        'Jonae':    'Jonah',
        'Nahum':    'Nahum',
        'Habac':    'Hab.',
        'Agae':     'Hag.',
        'Zach':     'Zech.',
        'Zachar':   'Zech.',
        'Malac':    'Mal.',
        'Malach':   'Mal.',
        'Judith':   'Judith',
        'Baruc':    'Bar.',
        'Baruch':   'Bar.',
        'Matt':     'Matt.',
        'Matth':    'Matt.',
        'Marc':     'Mark',
        'Luc':      'Luke',
        'Joan':     'John',
        'Joann':    'John',
        'Act':      'Acts',
        'Actor':    'Acts',
        'Rom':      'Rom.',
        'I Cor':    '1Cor.',
        'II Cor':   '2Cor.',
        'Gal':      'Gal.',
        'Galat':    'Gal.',
        'Ephes':    'Eph.',
        'Phil':     'Phil.',
        'Philip':   'Phil.',
        'Coloss':   'Col.',
        'Hebr':     'Heb.',
        'Heb':      'Heb.',
        'I Thess':  '1Th.',
        'II Thess': '2Th.',
        'I Tim':    '1Tim.',
        'II Tim':   '2Tim.',
        'II. Tim':  '2Tim.',
        'Tit':      'Tit.',
        'Jacob':    'James',
        'I Pet':    '1Pet.',
        'I Petr':   '1Pet.',
        'II Petr':  '2Pet.',
        'I Joan':   '1John',
        'Jud':      'Jude',
        'Apoc':     'Rev.',
    }
    ref = match.group(1)
    book = re.sub(r'\s+', ' ', match.group(2))
    chapter = roman.fromRoman(match.group(3))
    verse = match.group(4)
    if book in books:
        return f'<a href="{books[book]} {chapter}:{verse}">{ref}</a>'
    else:
        sys.stderr.write(f"Unrecognized book, try adding: '{book}':{' '*(9-len(book))}'{book}.',\n")
        return ref


def tag_scripture(soup):
    """Add Accordance links for Scripture references found in the soup.
    """
    regex = re.compile(r'(((?:[IV]+\.?\s+)?\w+)\.?\s+([CLXVI]+),\s+(\d+))')
    for s in soup.find_all(string=regex):
        tagged = re.sub(regex, mark_if_scripture, s)
        s.replace_with(BeautifulSoup(tagged, 'html.parser'))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type=open, help='Input file in default encoding.')
    parser.add_argument('outfile', type=argparse.FileType('w', encoding='utf-8'), help='Output file in UTF-8.')
    args = parser.parse_args()

    # As before, if I tell it it's XML, there's hardly any content.
    soup = BeautifulSoup(args.infile, 'html.parser')
    args.infile.close()
    content = convert(soup)
    tag_scripture(content)
    args.outfile.write(content.prettify(formatter='minimal'))
    args.outfile.close()

if __name__ == "__main__":
    main()
